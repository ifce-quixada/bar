""" basic tests """

import pytest
from bar import create_app

@pytest.fixture
def app():
    """ app fixture """
    app = create_app({
        'TESTING': True
    })

    with app.app_context():
        pass

    return app

@pytest.fixture
def client(app):
    """ client fixture """
    return app.test_client()

@pytest.fixture
def runner(app):
    """ runner fixture """
    return app.test_cli_runner()
