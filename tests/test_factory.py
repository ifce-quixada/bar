""" testing factory """

from bar import create_app

def test_config():
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing

def test_health(client):
    response = client.get('/health')
    assert response.status_code == 200
    assert response.data == b'OK'
