""" tests for persona api """
#!/usr/bin/env python

import pytest

def test_persona_index(client):
    """ test persona index method """
    response = client.get("/persona/")
    assert response.status_code == 200
    assert response.data == b'OK'

def test_persona_generate(client):
    """ test persona generate method """
    response = client.get("/persona/generate")
    assert response.status_code == 200
    assert response.data == b'generating a person'

def test_persona_generate_contact(client):
    """ test persona generate contact_information """
    response = client.get("/persona/generate/contact-information")
    assert response.status_code == 200
