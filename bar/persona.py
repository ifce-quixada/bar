""" blueprint file for persona module """
import functools

from flask import (
    Blueprint, flash, g, redirect, request, session, url_for
)

bp = Blueprint('persona', __name__, url_prefix='/persona')

@bp.route('/', methods=['GET'])
def index():
    """ list all personas that were generated """
    return "OK"

@bp.route('/generate', methods=['GET'])
def generate():
    """ generates a persona """
    return u'generating a person'

@bp.route('/generate/contact-information', methods=['GET'])
def contact_information():
    """ generates only the contacts information
    like phone, e-mail, address """
    return 'blah'
