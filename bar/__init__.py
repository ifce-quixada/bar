""" simple flask application """

import os
import uuid

from flask import Flask

from . import persona

def create_app(test_config=None):
    """ handle flask app creation """
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        FLASK_APP='bar',
        FLASK_ENVIRONMENT='development',
        SECRET_KEY=uuid.uuid4().hex
    )

    app.register_blueprint(persona.bp)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    @app.route('/health')
    def health():
        """ handle health requests """
        return 'OK'

    return app
